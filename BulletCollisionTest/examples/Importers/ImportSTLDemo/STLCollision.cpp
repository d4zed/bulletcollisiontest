#include "STLCollision.h"
#include <vector>
#include <chrono> // time measurement
#include <windows.h>

// general
#include "../OpenGLWindow/GLInstancingRenderer.h"
#include "../OpenGLWindow/GLInstanceGraphicsShape.h"
#include "../BulletCollision/btBulletDynamicsCommon.h"
#include "../OpenGLWindow/SimpleOpenGL3App.h"
#include "LoadMeshFromSTL.h"
#include "../CommonInterfaces/CommonRigidBodyBase.h"
#include "../../Utils/b3ResourcePath.h"
#include "../../Utils/b3BulletDefaultFileIO.h"

// MultiBody
#include "BulletDynamics/Featherstone/btMultiBody.h"
#include "BulletDynamics/Featherstone/btMultiBodyConstraintSolver.h"
#include "BulletDynamics/Featherstone/btMultiBodyMLCPConstraintSolver.h"
#include "BulletDynamics/Featherstone/btMultiBodyDynamicsWorld.h"
#include "BulletDynamics/Featherstone/btMultiBodyLinkCollider.h"
#include "BulletDynamics/Featherstone/btMultiBodyLink.h"
#include "BulletDynamics/Featherstone/btMultiBodyJointLimitConstraint.h"
#include "BulletDynamics/Featherstone/btMultiBodyJointMotor.h"
#include "BulletDynamics/Featherstone/btMultiBodyPoint2Point.h"
#include "BulletDynamics/Featherstone/btMultiBodyFixedConstraint.h"
#include "BulletDynamics/Featherstone/btMultiBodySliderConstraint.h"
#include "../CommonInterfaces/CommonParameterInterface.h"
#include "../CommonInterfaces/CommonMultiBodyBase.h"

//(complex) ImpactShape Collision
#include "../BulletCollision/Gimpact/btGImpactShape.h"
#include "../BulletCollision/Gimpact/btCompoundFromGimpact.h"
#include "../BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h"

static btScalar modelposition[63] = {0};
static btScalar lastmodelposition[63] = {0};
static int shapeId = 0;
static auto collisionDuration = 0;

class STLCollision : public CommonMultiBodyBase
{
	const char* m_fileName;
	btVector3 m_scaling;
	std::vector<btRigidBody*> collisionBodies;
	std::vector<btGImpactMeshShape*> collisionShapes;
	float timePassed = 0;
	bool enableRendering = false;


public:
	STLCollision(struct GUIHelperInterface* helper, const char* fileName);
	virtual ~STLCollision();

	virtual void initPhysics();
	virtual void stepSimulation(float deltaTime);
	virtual void renderScene();

	virtual void resetCamera()
	{

	}

	private:
	void RegisterGUI(GLInstanceGraphicsShape* gfxShape, int shapeIndex);
	void CreateXYZSlidersForShape(int shapeId, float initialPosition[4]);
};

STLCollision::STLCollision(struct GUIHelperInterface* helper, const char* fileName)
	: CommonMultiBodyBase(helper),
	  m_scaling(btVector3(0.01, 0.01, 0.01))
{

}

STLCollision::~STLCollision()
{
}

void STLCollision::initPhysics()
{
	//camera positioning
	//float dist = -200;
	//float pitch = -28;
	//float yaw = -136;
	//float targetPos[3] = {0.47, 50, -0.64};
	//m_guiHelper->resetCamera(dist, yaw, pitch, targetPos[0], targetPos[1], targetPos[2]);

	m_guiHelper->setUpAxis(1);
	this->createEmptyDynamicsWorld();

	btMultiBodyConstraintSolver* solver = new btMultiBodyConstraintSolver();
	m_broadphase = new btDbvtBroadphase();

	btMultiBodyDynamicsWorld* world = new btMultiBodyDynamicsWorld(m_dispatcher, m_broadphase, solver, m_collisionConfiguration);
	m_dynamicsWorld = world;
	m_solver = solver;
	m_dynamicsWorld->setGravity(btVector3(0., 0, 0.));

	btCollisionDispatcher* dispatcher = static_cast<btCollisionDispatcher*>(m_dynamicsWorld->getDispatcher());
	btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher);

	m_guiHelper->createPhysicsDebugDrawer(m_dynamicsWorld);
	m_dynamicsWorld->getSolverInfo().m_globalCfm = 1e-3;

	std::vector<char*> stlFilesNames = {
		"microscope\\TischGround.stl",
		"microscope\\TischX.stl",
		"microscope\\TischY.stl",
		"microscope\\simpleObjectiveOptimized.stl",
	};

	btVector3 shift(0, 0, 0);
	//	int index=10;

	for (size_t i = 0; i < stlFilesNames.size(); i++)
	{
		b3BulletDefaultFileIO fileIO;
		char relativeFileName[1024];
		if (!b3ResourcePath::findResourcePath(stlFilesNames[i], relativeFileName, 1024, 0))
		{
			b3Warning("Cannot find file %s\n", m_fileName);
			return;
		}

		GLInstanceGraphicsShape* gfxShape = LoadMeshFromSTL(relativeFileName, &fileIO);

		RegisterGUI(gfxShape, i);
	}

	m_guiHelper->autogenerateGraphicsObjects(m_dynamicsWorld);
}

class CommonExampleInterface* STLCollisionCreateFunc(struct CommonExampleOptions& options)
{
	return new STLCollision(options.m_guiHelper, options.m_fileName);
}

void STLCollision::CreateXYZSlidersForShape(int shapeId, float initialPosition[4])
{
	int modelSliderIndex = (shapeId - 1) * 3;
	modelposition[modelSliderIndex] = initialPosition[0];
	std::string idStr = "Model X " + std::to_string(shapeId);
	SliderParams sliderX(idStr.c_str(), &modelposition[modelSliderIndex]);
	sliderX.m_minVal = -50;
	sliderX.m_maxVal = 50;
	m_guiHelper->getParameterInterface()->registerSliderFloatParameter(sliderX);

	idStr = "Model Y " + std::to_string(shapeId);
	modelposition[modelSliderIndex + 1] = initialPosition[1];
	SliderParams sliderY(idStr.c_str(), &modelposition[modelSliderIndex + 1]);
	sliderY.m_minVal = -50;
	sliderY.m_maxVal = 50;
	m_guiHelper->getParameterInterface()->registerSliderFloatParameter(sliderY);

	idStr = "Model Z " + std::to_string(shapeId);
	modelposition[modelSliderIndex + 2] = initialPosition[2];
	SliderParams sliderZ(idStr.c_str(), &modelposition[modelSliderIndex + 2]);
	sliderZ.m_minVal = -50;
	sliderZ.m_maxVal = 50;
	m_guiHelper->getParameterInterface()->registerSliderFloatParameter(sliderZ);
}

/// <summary>
/// Registeres a loaded STL mesh into the GUI
/// </summary>
/// <param name="gfxShape"></param>
void STLCollision::RegisterGUI(GLInstanceGraphicsShape* gfxShape, int shapeIndex)
{
	int shapeId = m_guiHelper->registerGraphicsShape(&gfxShape->m_vertices->at(0).xyzw[0], gfxShape->m_numvertices, &gfxShape->m_indices->at(0), gfxShape->m_numIndices,
													 B3_GL_TRIANGLES, -1);

	//// Convert triangle mesh to Impact collision shape /////////////////////////////////////
	const GLInstanceVertex& v = gfxShape->m_vertices->at(0);
	btAlignedObjectArray<btVector3> convertedVerts;
	convertedVerts.reserve(gfxShape->m_numvertices);
	for (int i = 0; i < gfxShape->m_numvertices; i++)
	{
		convertedVerts.push_back(btVector3(
			gfxShape->m_vertices->at(i).xyzw[0],
			gfxShape->m_vertices->at(i).xyzw[1],
			gfxShape->m_vertices->at(i).xyzw[2]));
	}

	btTriangleMesh* meshInterface = new btTriangleMesh();
	for (int i = 0; i < gfxShape->m_numIndices / 3; i++)
	{
		const btVector3& v0 = convertedVerts[gfxShape->m_indices->at(i * 3 + 0)];
		const btVector3& v1 = convertedVerts[gfxShape->m_indices->at(i * 3 + 1)];
		const btVector3& v2 = convertedVerts[gfxShape->m_indices->at(i * 3 + 2)];
		meshInterface->addTriangle(v0, v1, v2);
	}
	btGImpactMeshShape* shape = new btGImpactMeshShape(meshInterface);
	///////////////////////////////////////////////////////////////////////////////////////////

	//// set shape scaling, mass, inerita, posiion to create a body from it  //////////////////
	btVector3 localScaling(m_scaling[0], m_scaling[1], m_scaling[2]);
	shape->setLocalScaling(localScaling);
	shape->updateBound();
	m_collisionShapes.push_back(shape);

	btTransform startTransform;
	startTransform.setIdentity();

	btScalar mass(1.0f);
	btVector3 localInertia;

	shape->calculateLocalInertia(mass, localInertia);

	float pos[4] = {0, 3.0 * shapeIndex, 0, 0};
	btVector3 position(pos[0], pos[1], pos[2]);
	startTransform.setOrigin(position);

	collisionBodies.push_back(createRigidBody(mass, startTransform, shape));
	collisionShapes.push_back(shape);
	///////////////////////////////////////////////////////////////////////////////////////////

	//// display collsion shape in GUI  ///////////////////////////////////////////////////////
	btTransform trans;
	trans.setIdentity();
	trans.setRotation(btQuaternion(btVector3(1, 0, 0), SIMD_HALF_PI));
	btQuaternion orn = trans.getRotation();
	btVector4 color(0, 0, 0, 1);
	color[shapeIndex % 3] = 1;

	int colShapeId = shapeId;
	shape->setUserIndex(colShapeId);
	int renderInstance = m_guiHelper->getRenderInterface()->registerGraphicsInstance(colShapeId, position, orn, color, m_scaling);
	collisionBodies[shapeIndex]->setUserIndex(renderInstance);
	///////////////////////////////////////////////////////////////////////////////////////////

	// create Slider for x, y, z movement
	CreateXYZSlidersForShape(shapeId, pos);
}

void STLCollision::renderScene()
{
	if (enableRendering)
	{
		CommonMultiBodyBase::renderScene();
	}
}

// callback to handle things in time steps
void STLCollision::stepSimulation(float deltaTime)
{
	if (m_dynamicsWorld)
	{
		bool positionChanged = false;

		for (size_t k = 0; k < collisionBodies.size() * 3; k++)
		{
			if (modelposition[k] != lastmodelposition[k])
			{
				positionChanged = true;
			}
		}

		// set position when slider value has changed
		if (positionChanged)
		{
			for (size_t i = 0; i < 4; i++)
			{
				btTransform tf;
				collisionBodies[i]->activate(false);
				tf = collisionBodies[i]->getWorldTransform();
				btVector3 startPos = tf.getOrigin();
				tf.setOrigin(btVector3(modelposition[i * 3], modelposition[i * 3 + 1], modelposition[i * 3 + 2]));
				collisionBodies[i]->setWorldTransform(tf);
			}

			auto start = std::chrono::high_resolution_clock::now();
			m_dynamicsWorld->performDiscreteCollisionDetection();
			auto end = std::chrono::high_resolution_clock::now();
			
			collisionDuration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
			std::string output = "collision time in microseconds: " + std::to_string(collisionDuration)+ '\n';
			printf(output.c_str());

			int numMani = m_dispatcher->getNumManifolds();
			
			if (numMani > 0)
			{
				for (size_t i = 0; i < 4; i++)
				{
					modelposition[i * 3] = lastmodelposition[i * 3];
					modelposition[i * 3 + 1] = lastmodelposition[i * 3 + 1];
					modelposition[i * 3 + 2] = lastmodelposition[i * 3 + 2];
				}
			}
		}

		for (size_t k = 0; k < collisionBodies.size() * 3; k++)
		{
			lastmodelposition[k] = modelposition[k];
		}

		//m_guiHelper->getAppInterface()->drawText("Collision Time: " + collisionDuration, 300, 30, 0.4f);
		CommonMultiBodyBase::stepSimulation(deltaTime);
	}
}

